%% SSE
% Es wird die Summe der Fehlerquadrate zwischen zwei Vektoren a und b berechnet
% indem elementweise die Quadrate der Differenzen zwischen a und b berechnet und
% danach aufsummiert werden.
%
%  Syntax:
%       err = sse(a, b)
%  Eingabe:
%       a, b    Vektoren
%  Ausgabe:
%       err     Summer der Fehlerquadrate von a und b

function err = sse(a, b)
%% Berechnung der Fehlerquadrate
% Die Berechnung der Summe der Fehlerquadrate $err$ erfolgt mit der Formel
%
% $$ err = \sum_i (a_i - b_i)^2 $$.

% Eingabevektoren in richtige Form bringen.
v1 = a(:);
v2 = b(:);
% Differenz berechnen
d = v1 - v2;
% Differenz elementweise quadrarieren und dann aufzusummieren
% (entspricht Skalarprodukt des Differerzvektors mit sich selbst)
err = d'*d;
end
