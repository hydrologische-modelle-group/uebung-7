%% Daten einlesen
baerenfels = importdata('Baerenfels.txt', '\t', 9);
Q = baerenfels.data(:, 6);
P = baerenfels.data(:, 7);

%% relevante Daten extrahieren
i1p = find(P>0, 1, 'first');
i2p = find(P>0, 1, 'last');
i1q = find(Q>0, 1, 'first');
i2q = find(Q>0, 1, 'last');
Q = Q(i1p:i2q);
P = P(i1p:i2p);
t_tmp = datetime([baerenfels.data(:, 1:5), zeros(size(baerenfels.data, 1), 1)]);
t_plot = t_tmp(i1p:i2q);
dt = 0.25; % 15 min = 0.25 h
t = (1:numel(Q))*dt;

%% Matrix fuer Einheitsganglinienverfahren erstellen
Pmat = fill_mat(P, Q);

%% Fit mit Einzellinearspeicher

% Zielfunktion definieren
%objf_els = @(par) sse(Q, Pmat*...);

% Startschaetzung fuer Parameter festlegen
%par0_els = ...;

% Optimierung mit fminsearch
%[par_opt_els, res_els] = fminsearch(..., ...);

% Berechnung der Modellvorhersage mit optimierten Parametern
%q_els = Pmat*els(..., ..., ...);

%% Fit mit linearer Speicherkaskade
% Zielfunktion definieren
%objf_lsk = @(par) sse(Q, Pmat*...);

% Startschaetzung fuer Parameter festlegen
%par0_lsk = ...;

% Optimierung mit fminsearch
%[par_opt_lsk, res_lsk] = fminsearch(..., ...);

% Zuweisen der Parameter in individuelle Variablen (optional)
%K_lsk = par_opt_lsk(...);
%N_lsk = par_opt_lsk(...);

% Berechnung der Modellvorhersage mit optimierten Parametern
%q_lsk = Pmat*lsk(..., ..., ..., ...);


%% Darstellung
fig = figure(1);
clf
plot(t_plot, Q)
hold all
plot(t_plot, q_els, "DisplayName", sprintf("Fit Einzellinearspeicher K=%f", par_opt_els))
plot(t_plot, q_lsk, "DisplayName", sprintf("Fit lineare Speicherkaskade K=%f N=%f", K_lsk, N_lsk))
xlabel("Datum in h")
ylabel("Durchfluss in m^3/s")
title("Hochwasserwelle Baerenfels/Poebelbach")

yyaxis right
hb = bar(t_tmp(i1p:i2p), P, "DisplayName", "Niederschlag P", "FaceColor", [0 0 1])
ax_right = gca();
ax_right.YDir = "reverse";
ax_right.YLim = [0 ax_right.YLim(2)*3];
ylabel("Niederschlag in m^3/s")

legend()


function [Pmat] = fill_mat(P, Q)
    NumQ = numel(Q);
    NumP = numel(P);
    NumH = NumQ-NumP+1;
    Pmat = zeros(NumH, NumQ);
    for i = 1:NumH
        Pmat(i:i+NumP-1, i) = P;
    end
end
