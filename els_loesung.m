%ELS    Einzellinearspeicher
%    Berechnet den Ausfluss aus einem Einzellinearspeicher.
%
%    Input:
%       t   Zeit (Array)
%       dt  Zeitschritt für Niederschlag / Input (Skalar)
%       K   Speicherkonstante
%
%    Output:
%       q   Ausfluss aus dem Speicher
%
%    Syntax:
%       q = els(t, dt, K)
function [q] = els_loesung(t, dt, K)
    Nt = numel(t);
    q = zeros(Nt, 1);
    for i=(1:Nt)
        if t(i)<= dt
            q(i) = (1 - exp(-t(i)/K));
        else
            q(i) = (exp((dt-t(i))/K) - exp(-t(i)/K));
        end
    end
end