%LSK    Lineare Speicherkaskade
%    Berechnet den Ausfluss aus einer lineare Speicherkaskade.
%
%    Input:
%       t   Zeit (Array)
%       dt  Zeitschritt für Niederschlag / Input (Skalar)
%       K   Speicherkonstante
%       N   Zahl der Speicher
%
%    Output:
%       q   Ausfluss aus dem Speicher
%
%    Syntax:
%       q = lsk(t, dt, K, N)
function [q] = lsk_loesung(t, dt, K, N)
    t = t(:);
    q = dt/(K.^(N).*gamma(N)).*t.^(N-1).*exp(-t./K);
end